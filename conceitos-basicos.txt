---Desenho Clean Architecture-------------------
>Entities (Enterprise Business Rules)
>>contém regra crítica e invariável da aplicação

>Use Cases (Application Business Rules)
>>Define o fluxo da aplicação


>Controllers; Gateways; Presenters (Interface Adapters)


>Devices; Web; UI; External Interfaces; DB (Frameworks & Drivers)
>>Formar de como os dados podem chegar
------------------------------------------------

---Use Case------------------------------
>Intenção.
>Clareza de cada comportamento do software.
>Detalhes não devem impactar nas regras de negócio.
>Frameworks, banco de dados, APIs, sistema de filas não devem impactar as regras.

>Use Cases - SRP
>>SRP: Single Responsability Principle (Princípio da responsabilidade única).
>>Temos a tendência de "reaproveitar" use cases por serem muito parecidos.
>>Ex: Alterar vs Inserir. Ambos consultam se o registro existe, persistem dados. MAS, são Use Cases diferentes. Por que?
>>SRP => Mudam por razões diferentes (significa que você está ferindo o princípio).
>>>Exemplo: Na inserção envie um e-mail e na alteração envia um e-mail diferente
-----------------------------------------

---Limites arquiteturais------------------------------
>Tudo que não impacta diretamente nas regras de negócios deve estar em um limite arquitetural diferente.
Ex: Não será o frontend, banco de dados que mudarão as regras de negócio da aplicação.
------------------------------------------------------

---Input vs Output------------------------------
>No final do dia, tudo se resume a um Input que retorna um Output
>>Ex: Criar um pedido (dados do pedido = input)
      Pedido criado (dados de retorno do pedido)
>Simplifique seu raciocínio ao criar um software sempre pensando em Input e Output.
------------------------------------------------

---DTO------------------------------------------
>DTO (Data Transfer Object)
>Trafegar dados entre os limites arquiteturais
>Objeto anêmico, sem comportamento
>Contém dados (Input ou Output)
>NÃO possui regras de negócio
>NÃO possui comportamento
>NÃO faz nada!

>API -> CONTROLLER -> USE CASE -> ENTITY
>Controller cria um DTO com os dados recebidos e envia para o Use Case
>Use Case executa seu fluxo, pega o resultado, cria um DTO para output e retorna para o Controller
------------------------------------------------

---Presenters-----------------------------------
>Objetos de transformação
>Adequa o DTO de output no formato correto para entregar o resultado
>Lembrando: Um sistema pode ter diversos formatos de entrega: ex: XML, JSON, Protobuf, GraphQL, CLI, etc.

EX:
>Estou na camada Controller
>Input = new CategoryInputDTO("name");
>Output = CreateCategoryUseCase(input);
>jsonResult = CategoryPresenter(output).toJson();
>xmlResult  = CategoryPresenter(output).toXML();
------------------------------------------------

---Entities vs DDD------------------------------
>Entities da Clean Architecture != Entities do DDD
>Clean Architecture define entity como camada de regras de negócio
>Elas se aplicam em qualquer situação.
>Não há definição explícita de como criar as entities.
>Normalmente utilizamos táticas do DDD.
>Entities = Agregados + Domain Services
------------------------------------------------