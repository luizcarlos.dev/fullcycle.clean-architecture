import Customer from "../../customer/entity/customer";
import CustomerChangeAddressEvent from "../../customer/event/customer-change-address.event";
import CustomerCreatedEvent from "../../customer/event/customer-created.event";
import EnviaConsoleLogHandler from "../../customer/event/handler/envia-console-log-handler";
import EnviaConsoleLog1Handler from "../../customer/event/handler/envia-console-log1-handler";
import EnviaConsoleLog2Handler from "../../customer/event/handler/envia-console-log2-handler";
import Address from "../../customer/value-object/address";
import SendEmailWhenProductIsCreatedHandler from "../../product/event/handler/send-email-when-product-is-created.handler";
import ProductCreatedEvent from "../../product/event/product-created.event";
import EventDispatcher from "./event-dispatcher";

describe("Domain events tests", () => {
  it("should register an event handler", () => {
    const eventDispatcher = new EventDispatcher();
    const eventHandler = new SendEmailWhenProductIsCreatedHandler();

    eventDispatcher.register("ProductCreatedEvent", eventHandler);

    expect(
      eventDispatcher.getEventHandlers["ProductCreatedEvent"]
    ).toBeDefined();
    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"].length).toBe(
      1
    );
    expect(
      eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]
    ).toMatchObject(eventHandler);
  });

  it("should unregister an event handler", () => {
    const eventDispatcher = new EventDispatcher();
    const eventHandler = new SendEmailWhenProductIsCreatedHandler();

    eventDispatcher.register("ProductCreatedEvent", eventHandler);

    expect(
      eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]
    ).toMatchObject(eventHandler);

    eventDispatcher.unregister("ProductCreatedEvent", eventHandler);

    expect(
      eventDispatcher.getEventHandlers["ProductCreatedEvent"]
    ).toBeDefined();
    expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"].length).toBe(
      0
    );
  });

  it("should unregister all event handlers", () => {
    const eventDispatcher = new EventDispatcher();
    const eventHandler = new SendEmailWhenProductIsCreatedHandler();

    eventDispatcher.register("ProductCreatedEvent", eventHandler);

    expect(
      eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]
    ).toMatchObject(eventHandler);

    eventDispatcher.unregisterAll();

    expect(
      eventDispatcher.getEventHandlers["ProductCreatedEvent"]
    ).toBeUndefined();
  });

  it("should notify all event handlers", () => {
    const eventDispatcher = new EventDispatcher();

    const eventHandlerSendEmailWhenProduct = new SendEmailWhenProductIsCreatedHandler();
    const spyEventHandlerSendEmailWhenProduct = jest.spyOn(eventHandlerSendEmailWhenProduct, "handle");
    eventDispatcher.register("ProductCreatedEvent", eventHandlerSendEmailWhenProduct);

    expect(
      eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]
    ).toMatchObject(eventHandlerSendEmailWhenProduct);

    const productCreatedEvent = new ProductCreatedEvent({
      name: "Product 1",
      description: "Product 1 description",
      price: 10.0,
    });

    // Quando o notify for executado o SendEmailWhenProductIsCreatedHandler.handle() deve ser chamado
    eventDispatcher.notify(productCreatedEvent);
    expect(spyEventHandlerSendEmailWhenProduct).toHaveBeenCalled();


    // #################### Eventos de Customer - Primeiro EVENTO - Customer Created ####################
    const eventHandlerEnviaConsoleLog1 = new EnviaConsoleLog1Handler();
    const spyEventHandlerEnviaConsoleLog1 = jest.spyOn(eventHandlerEnviaConsoleLog1, "handle");

    const eventHandlerEnviaConsoleLog2 = new EnviaConsoleLog2Handler();
    const spyEventHandlerEnviaConsoleLog2 = jest.spyOn(eventHandlerEnviaConsoleLog2, "handle");

    eventDispatcher.register("CustomerCreatedEvent", eventHandlerEnviaConsoleLog1);
    eventDispatcher.register("CustomerCreatedEvent", eventHandlerEnviaConsoleLog2);

    expect(
      eventDispatcher.getEventHandlers["CustomerCreatedEvent"][0]
    ).toMatchObject(eventHandlerEnviaConsoleLog1);

    expect(
      eventDispatcher.getEventHandlers["CustomerCreatedEvent"][1]
    ).toMatchObject(eventHandlerEnviaConsoleLog2);

    // Create Customer Event
    const customer = new Customer("1", "Customer 1");
    const address = new Address("Street 1", 123, "13330-250", "São Paulo");
    customer.Address = address;
    customer.activate();

    const customerCreatedEvent = new CustomerCreatedEvent(customer);

    // Quando o notify for executado o EnviaConsoleLog1Handler.handle() e EnviaConsoleLog2Handler.handle() deve ser chamado
    eventDispatcher.notify(customerCreatedEvent);
    expect(spyEventHandlerEnviaConsoleLog1).toHaveBeenCalled();
    expect(spyEventHandlerEnviaConsoleLog2).toHaveBeenCalled();



    // #################### Eventos de Customer - Segundo EVENTO - Change Endereço Customer ####################
    const eventHandlerEnviaConsoleLog = new EnviaConsoleLogHandler();
    const spyEventHandlerEnviaConsoleLog = jest.spyOn(eventHandlerEnviaConsoleLog, "handle");
    eventDispatcher.register("CustomerChangeAddressEvent", eventHandlerEnviaConsoleLog);

    expect(
      eventDispatcher.getEventHandlers["CustomerChangeAddressEvent"][0]
    ).toMatchObject(eventHandlerEnviaConsoleLog);

    // ChangeAddressEvent
    const address2 = new Address("Street 2", 2, "Zipcode 2", "City 2");
    customer.changeAddress(address2);


    const customerChangeAddressEvent = new CustomerChangeAddressEvent(customer);

    // Quando o notify for executado o EnviaConsoleLogHandler.handle() deve ser chamado
    eventDispatcher.notify(customerChangeAddressEvent);
    expect(spyEventHandlerEnviaConsoleLog).toHaveBeenCalled();
  });
});
