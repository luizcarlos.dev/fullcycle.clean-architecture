import Customer from "../../../domain/customer/entity/customer";
import CustomerRepositoryInterface from "../../../domain/customer/repository/customer-repository.interface";
import { InputListaCustomerDto, OutputListaCustomerDto } from "./list.customer.dto";

export default class ListCustomerUseCase {

    private customerRepository: CustomerRepositoryInterface;

    constructor(customerRepository: CustomerRepositoryInterface) {
        this.customerRepository = customerRepository;
    }

    async execute(input: InputListaCustomerDto): Promise<OutputListaCustomerDto> {
        const customers = await this.customerRepository.findAll();       
        return OutputMaper.toOutput(customers);
    }
}

class OutputMaper {
    static toOutput(customer: Customer[]): OutputListaCustomerDto {
       return {
            customers: customer.map((customer) => ({
                id: customer.id,
                name: customer.name,
                address: {
                    street: customer.Address.street,
                    number: customer.Address.number,
                    zip: customer.Address.zip,
                    city: customer.Address.city,
                },
            }))
        }; 
    }
}