import ProductFactory from "../../../domain/product/factory/product.factory";
import UpdateProductUseCase from "./update.product.usecase";

const product = ProductFactory.createProduct("Product 1", 15.99);

const input = {
    id: product.id,
    name: product.name,
    price: product.price
};

const MockRepository = () => {
    return {
        create: jest.fn(),
        findAll: jest.fn(),
        find: jest.fn().mockReturnValue(Promise.resolve(product)),
        update: jest.fn(),
    };
}

describe("Unit test for product update use case", () => {

    it ("should update a customer", async () => {
        const productRepository = MockRepository();
        const productUpdateUseCase = new UpdateProductUseCase(productRepository);

        input.name = "Product Update";
        input.price = 15;
        const output = await productUpdateUseCase.execute(input);

        expect(output).toEqual(input);
        expect(output.name).toBe(input.name);
        expect(output.price).toBe(input.price);
    })
})