import { Sequelize } from "sequelize-typescript";
import ProductFactory from "../../../domain/product/factory/product.factory";
import ProductModel from "../../../infrastructure/product/repository/sequelize/product.model";
import ProductRepository from "../../../infrastructure/product/repository/sequelize/product.repository";
import UpdateProductUseCase from "./update.product.usecase";

const product = ProductFactory.createProduct("Product 1", 15.99);

const input = {
    id: product.id,
    name: product.name,
    price: product.price
};

describe("Integration test for product update use case", () => {

    let sequelize: Sequelize;
    
    beforeEach(async () => {
        sequelize = new Sequelize({
            dialect: "sqlite",
            storage: ":memory:",
            logging: false,
            sync: { force: true },
        });
        
        await sequelize.addModels([ProductModel]);
        await sequelize.sync();
    });
    
    afterEach(async () => {
        await sequelize.close();
    });

    it("should update a customer", async () => {
        const productRepository = new ProductRepository();
        const productUpdateUseCase = new UpdateProductUseCase(productRepository);

        await productRepository.create(product);

        input.name = "Product Update";
        input.price = 15;

        const output = await productUpdateUseCase.execute(input);
        expect(output).toEqual(input);
        expect(output.name).toBe(input.name);
        expect(output.price).toBe(input.price);
    });

});