export interface InputListaProdutctDto {}

type Product = {

    id: string;
    name: string;
    price: number;
    
};

export interface OutputListaProductDto {
    products: Product[];
}