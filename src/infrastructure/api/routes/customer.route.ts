import express, {Request, Response} from 'express';
import CreateCustomerUseCase from '../../../usecase/customer/create/create.customer.usecase';
import CreateCustomerUserCase from '../../../usecase/customer/create/create.customer.usecase';
import ListCustomerUseCase from '../../../usecase/customer/list/list.customer.usecase';
import CustomerRepository from '../../customer/repository/sequelize/customer.repository';
import CustomerPresenter from '../presenters/customer.presenter';

export const customerRoute = express.Router();

customerRoute.post("/", async (req: Request, res: Response) => {
    const useCase = new CreateCustomerUseCase(new CustomerRepository());
    try {
        const customerDto = {
            name: req.body.name,
            address: {
                street: req.body.address.street,
                city: req.body.address.city,
                number: req.body.address.number,
                zip: req.body.address.zip,
            },
        }

        const output = await useCase.execute(customerDto);
        res.status(200).send(output);
    } catch (err) {
        console.log("######################");
        console.log(err);
        res.status(500).send(err);
    }
});

customerRoute.get("/", async (req: Request, res: Response) => {
    const useCase = new ListCustomerUseCase(new CustomerRepository());
    try {
        // Presenter:
        // O retorno do DTO <> Resultado API
        // Tudo é JSON. E se eu quiser o resultado em XML?
        // Kafka

        const output = await useCase.execute({});
        res.format({
            json: async () => res.send(output),
            xml: async () => res.send(CustomerPresenter.listXML(output)),
        });

    } catch (err) {
        console.log("######################");
        console.log(err);
        res.status(500).send(err);
    }
});