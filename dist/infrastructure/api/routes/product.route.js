"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.productRoute = void 0;
const express_1 = __importDefault(require("express"));
const create_product_usecase_1 = __importDefault(require("../../../usecase/product/create/create.product.usecase"));
const list_product_usecase_1 = __importDefault(require("../../../usecase/product/list/list.product.usecase"));
const product_repository_1 = __importDefault(require("../../product/repository/sequelize/product.repository"));
exports.productRoute = express_1.default.Router();
exports.productRoute.post("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const useCase = new create_product_usecase_1.default(new product_repository_1.default());
    try {
        const productDto = {
            name: req.body.name,
            price: req.body.price,
        };
        const output = yield useCase.execute(productDto);
        res.status(200).send(output);
    }
    catch (err) {
        console.log("######################");
        console.log(err);
        res.status(500).send(err);
    }
}));
exports.productRoute.get("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const useCase = new list_product_usecase_1.default(new product_repository_1.default());
    try {
        const output = yield useCase.execute({});
        res.status(200).send(output);
    }
    catch (err) {
        console.log("######################");
        console.log(err);
        res.status(500).send(err);
    }
}));
