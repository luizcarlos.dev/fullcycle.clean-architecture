"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("../express");
const supertest_1 = __importDefault(require("supertest"));
describe("E2E test for product", () => {
    beforeEach(() => __awaiter(void 0, void 0, void 0, function* () {
        yield express_1.sequelize.sync({ force: true });
    }));
    afterAll(() => __awaiter(void 0, void 0, void 0, function* () {
        yield express_1.sequelize.close();
    }));
    it("should create a product", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(express_1.app)
            .post("/product")
            .send({
            name: "Product 1",
            price: 123,
        }).expect(200);
        //expect(response.status).toBe(200);
        expect(response.body.name).toBe("Product 1");
        expect(response.body.price).toBe(123);
    }));
    it("should not create a product", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(express_1.app)
            .post("/product")
            .send({
            name: "Product 1"
        }).expect(500);
        //expect(response.status).toBe(500);
    }));
    it("should list all product", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(express_1.app)
            .post("/product")
            .send({
            name: "Product 1",
            price: 123
        }).expect(200);
        //expect(response.status).toBe(200);
        const response2 = yield (0, supertest_1.default)(express_1.app)
            .post("/product")
            .send({
            name: "Product 2",
            price: 500.25
        }).expect(200);
        //expect(response2.status).toBe(200);
        const listResponse = yield (0, supertest_1.default)(express_1.app).get("/product").send().expect(200);
        //expect(listResponse.status).toBe(200);
        expect(listResponse.body.products.length).toBe(2);
        const product1 = listResponse.body.products[0];
        expect(product1.name).toBe("Product 1");
        expect(product1.price).toBe(123);
        const product2 = listResponse.body.products[1];
        expect(product2.name).toBe("Product 2");
        expect(product2.price).toBe(500.25);
    }));
});
