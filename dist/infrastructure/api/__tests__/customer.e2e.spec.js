"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("../express");
const supertest_1 = __importDefault(require("supertest"));
describe("E2E test for customer", () => {
    beforeEach(() => __awaiter(void 0, void 0, void 0, function* () {
        yield express_1.sequelize.sync({ force: true });
    }));
    afterAll(() => __awaiter(void 0, void 0, void 0, function* () {
        yield express_1.sequelize.close();
    }));
    it("should create a customer", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(express_1.app)
            .post("/customer")
            .send({
            name: "John",
            address: {
                street: "Street",
                city: "City",
                number: 123,
                zip: "12345"
            }
        }).expect(200);
        //expect(response.status).toBe(200);
        expect(response.body.name).toBe("John");
        expect(response.body.address.street).toBe("Street");
        expect(response.body.address.city).toBe("City");
        expect(response.body.address.number).toBe(123);
        expect(response.body.address.zip).toBe("12345");
    }));
    it("should not create a customer", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(express_1.app)
            .post("/customer")
            .send({
            name: "John"
        }).expect(500);
        //expect(response.status).toBe(500);
    }));
    it("should list all customer", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield (0, supertest_1.default)(express_1.app)
            .post("/customer")
            .send({
            name: "John",
            address: {
                street: "Street",
                city: "City",
                number: 123,
                zip: "12345"
            }
        }).expect(200);
        //expect(response.status).toBe(200);
        const response2 = yield (0, supertest_1.default)(express_1.app)
            .post("/customer")
            .send({
            name: "Jane",
            address: {
                street: "Street 2",
                city: "City 2",
                number: 321,
                zip: "54321"
            }
        }).expect(200);
        //expect(response2.status).toBe(200);
        const listResponse = yield (0, supertest_1.default)(express_1.app).get("/customer").send().expect(200);
        //expect(listResponse.status).toBe(200);
        expect(listResponse.body.customers.length).toBe(2);
        const customer1 = listResponse.body.customers[0];
        expect(customer1.name).toBe("John");
        expect(customer1.address.street).toBe("Street");
        const customer2 = listResponse.body.customers[1];
        expect(customer2.name).toBe("Jane");
        expect(customer2.address.street).toBe("Street 2");
    }));
});
