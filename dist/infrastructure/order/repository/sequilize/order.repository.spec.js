"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_typescript_1 = require("sequelize-typescript");
const order_1 = __importDefault(require("../../../../domain/checkout/entity/order"));
const order_item_1 = __importDefault(require("../../../../domain/checkout/entity/order_item"));
const customer_1 = __importDefault(require("../../../../domain/customer/entity/customer"));
const address_1 = __importDefault(require("../../../../domain/customer/value-object/address"));
const product_1 = __importDefault(require("../../../../domain/product/entity/product"));
const customer_model_1 = __importDefault(require("../../../customer/repository/sequelize/customer.model"));
const customer_repository_1 = __importDefault(require("../../../customer/repository/sequelize/customer.repository"));
const product_model_1 = __importDefault(require("../../../product/repository/sequelize/product.model"));
const product_repository_1 = __importDefault(require("../../../product/repository/sequelize/product.repository"));
const order_item_model_1 = __importDefault(require("./order-item.model"));
const order_model_1 = __importDefault(require("./order.model"));
const order_repository_1 = __importDefault(require("./order.repository"));
describe("Order repository test", () => {
    let sequelize;
    beforeEach(() => __awaiter(void 0, void 0, void 0, function* () {
        sequelize = new sequelize_typescript_1.Sequelize({
            dialect: "sqlite",
            storage: ":memory:",
            logging: false,
            sync: { force: true },
        });
        yield sequelize.addModels([
            customer_model_1.default,
            order_model_1.default,
            order_item_model_1.default,
            product_model_1.default,
        ]);
        yield sequelize.sync();
    }));
    afterEach(() => __awaiter(void 0, void 0, void 0, function* () {
        yield sequelize.close();
    }));
    it("should create a new order", () => __awaiter(void 0, void 0, void 0, function* () {
        const customerRepository = new customer_repository_1.default();
        const customer = new customer_1.default("123", "Customer 1");
        const address = new address_1.default("Street 1", 1, "Zipcode 1", "City 1");
        customer.changeAddress(address);
        yield customerRepository.create(customer);
        const productRepository = new product_repository_1.default();
        const product = new product_1.default("123", "Product 1", 10);
        yield productRepository.create(product);
        const ordemItem = new order_item_1.default("1", product.name, product.price, product.id, 2);
        const order = new order_1.default("123", "123", [ordemItem]);
        const orderRepository = new order_repository_1.default();
        yield orderRepository.create(order);
        const orderModel = yield order_model_1.default.findOne({
            where: { id: order.id },
            include: ["items"],
        });
        expect(orderModel.toJSON()).toStrictEqual({
            id: "123",
            customer_id: "123",
            total: order.total(),
            items: [
                {
                    id: ordemItem.id,
                    name: ordemItem.name,
                    price: ordemItem.price,
                    quantity: ordemItem.quantity,
                    order_id: "123",
                    product_id: "123",
                },
            ],
        });
    }));
    it("should update a order", () => __awaiter(void 0, void 0, void 0, function* () {
        // CREATE
        const customerRepository = new customer_repository_1.default();
        const customer = new customer_1.default("123", "Customer 1");
        const address = new address_1.default("Street 1", 1, "Zipcode 1", "City 1");
        customer.changeAddress(address);
        yield customerRepository.create(customer);
        const productRepository = new product_repository_1.default();
        const product = new product_1.default("123", "Product 1", 10);
        yield productRepository.create(product);
        const ordemItem = new order_item_1.default("1", product.name, product.price, product.id, 2);
        const order = new order_1.default("123", customer.id, [ordemItem]);
        const orderRepository = new order_repository_1.default();
        yield orderRepository.create(order);
        // UPDATE
        const product2 = new product_1.default("2", "Product 2", 20);
        yield productRepository.create(product2);
        const ordemItem2 = new order_item_1.default("2", product2.name, product2.price, product2.id, 3);
        order.changeItens([ordemItem, ordemItem2]);
        yield orderRepository.update(order);
        const orderModel = yield order_model_1.default.findOne({
            where: { id: order.id },
            include: ["items"],
        });
        expect(orderModel.toJSON()).toStrictEqual({
            id: "123",
            customer_id: "123",
            total: order.total(),
            items: [
                {
                    id: ordemItem.id,
                    name: ordemItem.name,
                    price: ordemItem.price,
                    quantity: ordemItem.quantity,
                    order_id: order.id,
                    product_id: product.id,
                },
                {
                    id: ordemItem2.id,
                    name: ordemItem2.name,
                    price: ordemItem2.price,
                    quantity: ordemItem2.quantity,
                    order_id: order.id,
                    product_id: product2.id,
                },
            ],
        });
    }));
    it("should find a order", () => __awaiter(void 0, void 0, void 0, function* () {
        const customerRepository = new customer_repository_1.default();
        const customer = new customer_1.default("123", "Customer 1");
        const address = new address_1.default("Street 1", 1, "Zipcode 1", "City 1");
        customer.changeAddress(address);
        yield customerRepository.create(customer);
        const productRepository = new product_repository_1.default();
        const product = new product_1.default("123", "Product 1", 10);
        yield productRepository.create(product);
        const ordemItem = new order_item_1.default("1", product.name, product.price, product.id, 2);
        const order = new order_1.default("123", customer.id, [ordemItem]);
        const orderRepository = new order_repository_1.default();
        yield orderRepository.create(order);
        const orderResult = yield orderRepository.find(order.id);
        expect(order).toStrictEqual(orderResult);
    }));
    it("should find all orders", () => __awaiter(void 0, void 0, void 0, function* () {
        // Order 1
        const customerRepository = new customer_repository_1.default();
        const customer1 = new customer_1.default("1", "Customer 1");
        const address1 = new address_1.default("Street 1", 1, "Zipcode 1", "City 1");
        customer1.changeAddress(address1);
        yield customerRepository.create(customer1);
        const productRepository = new product_repository_1.default();
        const product1 = new product_1.default("1", "Product 1", 10);
        yield productRepository.create(product1);
        const ordemItem1 = new order_item_1.default("1", product1.name, product1.price, product1.id, 2);
        const order1 = new order_1.default("1", customer1.id, [ordemItem1]);
        // Order 2
        const customer2 = new customer_1.default("2", "Customer 1");
        const address2 = new address_1.default("Street 2", 2, "Zipcode 2", "City 2");
        customer2.changeAddress(address2);
        yield customerRepository.create(customer2);
        const product2 = new product_1.default("2", "Product 2", 20);
        yield productRepository.create(product2);
        const ordemItem2 = new order_item_1.default("2", product2.name, product2.price, product2.id, 2);
        const product3 = new product_1.default("3", "Product 3", 30);
        yield productRepository.create(product3);
        const ordemItem3 = new order_item_1.default("3", product3.name, product3.price, product3.id, 3);
        const order2 = new order_1.default("2", customer2.id, [ordemItem2, ordemItem3]);
        // Persistência de Order
        const orderRepository = new order_repository_1.default();
        yield orderRepository.create(order1);
        yield orderRepository.create(order2);
        const orders = yield orderRepository.findAll();
        expect(orders).toHaveLength(2);
        expect(orders).toContainEqual(order1);
        expect(orders).toContainEqual(order2);
    }));
});
