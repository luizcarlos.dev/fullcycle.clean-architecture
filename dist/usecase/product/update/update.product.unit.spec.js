"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const product_factory_1 = __importDefault(require("../../../domain/product/factory/product.factory"));
const update_product_usecase_1 = __importDefault(require("./update.product.usecase"));
const product = product_factory_1.default.createProduct("Product 1", 15.99);
const input = {
    id: product.id,
    name: product.name,
    price: product.price
};
const MockRepository = () => {
    return {
        create: jest.fn(),
        findAll: jest.fn(),
        find: jest.fn().mockReturnValue(Promise.resolve(product)),
        update: jest.fn(),
    };
};
describe("Unit test for product update use case", () => {
    it("should update a customer", () => __awaiter(void 0, void 0, void 0, function* () {
        const productRepository = MockRepository();
        const productUpdateUseCase = new update_product_usecase_1.default(productRepository);
        input.name = "Product Update";
        input.price = 15;
        const output = yield productUpdateUseCase.execute(input);
        expect(output).toEqual(input);
        expect(output.name).toBe(input.name);
        expect(output.price).toBe(input.price);
    }));
});
