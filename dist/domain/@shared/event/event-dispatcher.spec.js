"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const customer_1 = __importDefault(require("../../customer/entity/customer"));
const customer_change_address_event_1 = __importDefault(require("../../customer/event/customer-change-address.event"));
const customer_created_event_1 = __importDefault(require("../../customer/event/customer-created.event"));
const envia_console_log_handler_1 = __importDefault(require("../../customer/event/handler/envia-console-log-handler"));
const envia_console_log1_handler_1 = __importDefault(require("../../customer/event/handler/envia-console-log1-handler"));
const envia_console_log2_handler_1 = __importDefault(require("../../customer/event/handler/envia-console-log2-handler"));
const address_1 = __importDefault(require("../../customer/value-object/address"));
const send_email_when_product_is_created_handler_1 = __importDefault(require("../../product/event/handler/send-email-when-product-is-created.handler"));
const product_created_event_1 = __importDefault(require("../../product/event/product-created.event"));
const event_dispatcher_1 = __importDefault(require("./event-dispatcher"));
describe("Domain events tests", () => {
    it("should register an event handler", () => {
        const eventDispatcher = new event_dispatcher_1.default();
        const eventHandler = new send_email_when_product_is_created_handler_1.default();
        eventDispatcher.register("ProductCreatedEvent", eventHandler);
        expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"]).toBeDefined();
        expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"].length).toBe(1);
        expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]).toMatchObject(eventHandler);
    });
    it("should unregister an event handler", () => {
        const eventDispatcher = new event_dispatcher_1.default();
        const eventHandler = new send_email_when_product_is_created_handler_1.default();
        eventDispatcher.register("ProductCreatedEvent", eventHandler);
        expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]).toMatchObject(eventHandler);
        eventDispatcher.unregister("ProductCreatedEvent", eventHandler);
        expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"]).toBeDefined();
        expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"].length).toBe(0);
    });
    it("should unregister all event handlers", () => {
        const eventDispatcher = new event_dispatcher_1.default();
        const eventHandler = new send_email_when_product_is_created_handler_1.default();
        eventDispatcher.register("ProductCreatedEvent", eventHandler);
        expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]).toMatchObject(eventHandler);
        eventDispatcher.unregisterAll();
        expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"]).toBeUndefined();
    });
    it("should notify all event handlers", () => {
        const eventDispatcher = new event_dispatcher_1.default();
        const eventHandlerSendEmailWhenProduct = new send_email_when_product_is_created_handler_1.default();
        const spyEventHandlerSendEmailWhenProduct = jest.spyOn(eventHandlerSendEmailWhenProduct, "handle");
        eventDispatcher.register("ProductCreatedEvent", eventHandlerSendEmailWhenProduct);
        expect(eventDispatcher.getEventHandlers["ProductCreatedEvent"][0]).toMatchObject(eventHandlerSendEmailWhenProduct);
        const productCreatedEvent = new product_created_event_1.default({
            name: "Product 1",
            description: "Product 1 description",
            price: 10.0,
        });
        // Quando o notify for executado o SendEmailWhenProductIsCreatedHandler.handle() deve ser chamado
        eventDispatcher.notify(productCreatedEvent);
        expect(spyEventHandlerSendEmailWhenProduct).toHaveBeenCalled();
        // #################### Eventos de Customer - Primeiro EVENTO - Customer Created ####################
        const eventHandlerEnviaConsoleLog1 = new envia_console_log1_handler_1.default();
        const spyEventHandlerEnviaConsoleLog1 = jest.spyOn(eventHandlerEnviaConsoleLog1, "handle");
        const eventHandlerEnviaConsoleLog2 = new envia_console_log2_handler_1.default();
        const spyEventHandlerEnviaConsoleLog2 = jest.spyOn(eventHandlerEnviaConsoleLog2, "handle");
        eventDispatcher.register("CustomerCreatedEvent", eventHandlerEnviaConsoleLog1);
        eventDispatcher.register("CustomerCreatedEvent", eventHandlerEnviaConsoleLog2);
        expect(eventDispatcher.getEventHandlers["CustomerCreatedEvent"][0]).toMatchObject(eventHandlerEnviaConsoleLog1);
        expect(eventDispatcher.getEventHandlers["CustomerCreatedEvent"][1]).toMatchObject(eventHandlerEnviaConsoleLog2);
        // Create Customer Event
        const customer = new customer_1.default("1", "Customer 1");
        const address = new address_1.default("Street 1", 123, "13330-250", "São Paulo");
        customer.Address = address;
        customer.activate();
        const customerCreatedEvent = new customer_created_event_1.default(customer);
        // Quando o notify for executado o EnviaConsoleLog1Handler.handle() e EnviaConsoleLog2Handler.handle() deve ser chamado
        eventDispatcher.notify(customerCreatedEvent);
        expect(spyEventHandlerEnviaConsoleLog1).toHaveBeenCalled();
        expect(spyEventHandlerEnviaConsoleLog2).toHaveBeenCalled();
        // #################### Eventos de Customer - Segundo EVENTO - Change Endereço Customer ####################
        const eventHandlerEnviaConsoleLog = new envia_console_log_handler_1.default();
        const spyEventHandlerEnviaConsoleLog = jest.spyOn(eventHandlerEnviaConsoleLog, "handle");
        eventDispatcher.register("CustomerChangeAddressEvent", eventHandlerEnviaConsoleLog);
        expect(eventDispatcher.getEventHandlers["CustomerChangeAddressEvent"][0]).toMatchObject(eventHandlerEnviaConsoleLog);
        // ChangeAddressEvent
        const address2 = new address_1.default("Street 2", 2, "Zipcode 2", "City 2");
        customer.changeAddress(address2);
        const customerChangeAddressEvent = new customer_change_address_event_1.default(customer);
        // Quando o notify for executado o EnviaConsoleLogHandler.handle() deve ser chamado
        eventDispatcher.notify(customerChangeAddressEvent);
        expect(spyEventHandlerEnviaConsoleLog).toHaveBeenCalled();
    });
});
