"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const entity_abstract_1 = __importDefault(require("../../@shared/entity/entity.abstract"));
const notification_error_1 = __importDefault(require("../../@shared/notification/notification.error"));
class Product extends entity_abstract_1.default {
    constructor(id, name, price) {
        super();
        this._id = id;
        this._name = name;
        this._price = price;
        this.validate();
    }
    get id() {
        return this._id;
    }
    get name() {
        return this._name;
    }
    get price() {
        return this._price;
    }
    changeName(name) {
        this._name = name;
        this.validate();
    }
    changePrice(price) {
        this._price = price;
        this.validate();
    }
    validate() {
        if (this._id.length === 0) {
            this.notification.addError({
                context: "product",
                message: "Id is required"
            });
        }
        if (this._name.length === 0) {
            this.notification.addError({
                context: "product",
                message: "Name is required"
            });
        }
        if (this._price < 0) {
            this.notification.addError({
                context: "product",
                message: "Price must be greater than zero"
            });
        }
        if (this.notification.hasErrors()) {
            throw new notification_error_1.default(this.notification.getErrors());
        }
        return true;
    }
}
exports.default = Product;
