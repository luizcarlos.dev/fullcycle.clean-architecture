"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EnviaConsoleLogHandler {
    handle(event) {
        const customer = event.eventData;
        console.log(`Endereço do cliente: ID Customer: ${customer.id}, Customer: ${customer.name}, alterado para: ${customer.Address.toString()}`);
    }
}
exports.default = EnviaConsoleLogHandler;
