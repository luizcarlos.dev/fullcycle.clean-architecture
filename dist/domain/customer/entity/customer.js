"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const entity_abstract_1 = __importDefault(require("../../@shared/entity/entity.abstract"));
const notification_error_1 = __importDefault(require("../../@shared/notification/notification.error"));
class Customer extends entity_abstract_1.default {
    constructor(id, name) {
        super();
        this._name = "";
        this._active = false;
        this._rewardPoints = 0;
        this._id = id;
        this._name = name;
        this.validate();
        if (this.notification.hasErrors()) {
            throw new notification_error_1.default(this.notification.getErrors());
        }
    }
    get id() {
        return this._id;
    }
    get name() {
        return this._name;
    }
    get rewardPoints() {
        return this._rewardPoints;
    }
    validate() {
        if (this.id.length === 0) {
            this.notification.addError({
                context: "customer",
                message: "Id is required",
            });
        }
        if (this._name.length === 0) {
            this.notification.addError({
                context: "customer",
                message: "Name is required",
            });
        }
    }
    changeName(name) {
        this._name = name;
        this.validate();
    }
    get Address() {
        return this._address;
    }
    changeAddress(address) {
        this._address = address;
    }
    isActive() {
        return this._active;
    }
    activate() {
        if (this._address === undefined) {
            throw new Error("Address is mandatory to activate a customer");
        }
        this._active = true;
    }
    deactivate() {
        this._active = false;
    }
    addRewardPoints(points) {
        this._rewardPoints += points;
    }
    set Address(address) {
        this._address = address;
    }
}
exports.default = Customer;
